class LoupGarou():
# {{{
    def __init__(self, thiercelieux):
        self.players = []
        self.dead = []
        self.victim = []
        self.thiercelieux = thiercelieux

    def play(self):
        pass

    @property
    def alive(self):
        return len(self.players) == len(self.dead)
# }}}

class Villageois():
# {{{
    def __init__(self, thiercelieux):
        self.players = []
        self.dead = []
        self.thiercelieux = thiercelieux

    def play(self):
        pass

    @property
    def alive(self):
        return len(self.players) == len(self.dead)
# }}}

class Sorciere():
# {{{
    def __init__(self, thiercelieux):
        self.player = ""
        self.thiercelieux = thiercelieux

    def play(self):
        pass
# }}}

class Voyante():
# {{{
    def __init__(self, thiercelieux):
        self.player = ""
        self.thiercelieux = thiercelieux

    def play(self):
        pass
# }}}

class Cupidon():
# {{{
    def __init__(self, thiercelieux):
        self.player = ""
        self.thiercelieux = thiercelieux

    def play(self):
        pass
# }}}

class Salvateur():
# {{{
    def __init__(self, thiercelieux):
        self.player = ""
        self.thiercelieux = thiercelieux

    def play(self):
        pass
# }}}

class Flute():
# {{{
    def __init__(self, thiercelieux):
        self.player = ""
        self.thiercelieux = thiercelieux

    def play(self):
        pass
# }}}

class Chasseur():
# {{{
    def __init__(self, thiercelieux):
        self.player = ""
        self.thiercelieux = thiercelieux

    def play(self):
        pass
# }}}

ABSOLUTE_ORDER = [Cupidon,
        Salvateur,
        Voyante,
        LoupGarou,
        Sorciere,
        Flute,
        Villageois,
        Chasseur
        ]

FIXED_CARDS = ["LoupGarou",
        "Villageois"
        ]

# vim:set fdm=marker:

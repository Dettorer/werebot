#! /usr/bin/env python3
# -*- coding: utf8

from pypeul import IRC
from datetime import datetime
import yaml
import cards
import random
import codecs

with codecs.open('conf.yml', 'r', 'utf-8') as stream:
    CONF = yaml.load(stream)

IRC_CONF = CONF['irc']['test']
MSG = CONF['messages']
DEFAULT_SET = [cards.Villageois, cards.LoupGarou, cards.Voyante, cards.Sorciere]
LGRATE = CONF['constants']['lgrate']

def rdc(msg):
    return random.choice(MSG[msg])

class Thiercelieux(IRC):
    ''' This class handles the general game's mechanism and irc bot '''

# {{{ init
    def __init__(self):
        self.card_set = DEFAULT_SET
        self.card_dict = {}
        self.players = []
        self.ingame = False

        super().__init__()
        self.connect(IRC_CONF['server'])
        self.ident(IRC_CONF['nick'])
        self.channel = IRC_CONF['channel']
        self.channel_key = IRC_CONF['channel_key']
        self.run()

    def on_ready(self):
        self.join(self.channel, self.channel_key)
        self.message('nickserv', 'identify ' + IRC_CONF['nick_key'])
        self.message(self.channel, 'Bien le bonjour.')
# }}}

# {{{ message handling
    def on_message(self, umask, channel, msg):
        t = datetime.now().strftime("%H:%M:%S")
        print('{} [{}] {}: {}'.format(t, channel, umask, msg))
        if self.ingame:
            self.handle_message_ingame(umask, channel, msg)
        else:
            self.handle_message_wait(umask, channel, msg)

    def handle_message_wait(self, umask, channel, msg):
        command, *args = msg.split()

        if channel != self.channel:
            return

        if command == '!cards':
            self.list_cards()
        elif command == '!play':
            self.add_player(umask)
        elif command == '!unplay':
            self.remove_player(umask)
        elif command == '!players':
            self.list_players()
        elif command == '!add_card':
            self.add_card(args[0])
        elif command == '!remove_card':
            self.remove_card(args[0])
        elif command == '!start':
            self.run_game()

    def handle_message_ingame(self, umask, channel, msg):
        pass

# }}}

# {{{ player commands
    @property
    def minimum_players(self):
        ''' Calculate the minimum player number needed for the current card set '''
        # Loups-garous
        n = 1 + int(len(self.players) / LGRATE)
        # If next player must be a LoupGarou
        if (len(self.players) + 1) % LGRATE == 0:
            n += 1
        # Plus one per card (ignoring LG and Villageois)
        n += len(self.card_set) - 2
        return n

    def add_player(self, umask):
        if umask.nick not in self.players:
            self.players.append(umask.nick)
            self.message(self.channel, "Je note {} pour la prochaine partie".format(umask.nick))
            # TODO: voice player
        else:
            self.message(self.channel, "{}: Vous jouez déjà. Si vous êtes nul: !unplay".format(umask.nick))

    def remove_player(self, umask):
        try:
            del self.players[self.players.index(umask.nick)]
            self.message(self.channel, "{} n'est pas quelqu'un de très rigolo".format(umask.nick))
        except ValueError:
            self.message(self.channel, "{}: Vous ne jouez déjà pas, !play pour jouer".format(umask.nick))
        # TODO: unvoice player

    def list_players(self):
        if len(self.players) == 0:
            msg = "Personne ne joue, on se motive !"
        else:
            msg = "Prêts à jouer {} ?".format(", ".join(self.players))

        delta_players = self.minimum_players - len(self.players)
        if delta_players > 0:
            msg += "\nIl manque {} joueurs.".format(delta_players)
            if len(self.card_set) > 2:
                msg += " Vous pouvez aussi retirer des cartes (!cards pour afficher les cartes sélectionnées)"
        self.message(self.channel, msg)
# }}}

# {{{ card commands
    def remove_card(self, card):
        if card in cards.FIXED_CARDS:
            self.message(self.channel, "Cette carte est obligatoire")
            return

        try:
            del self.card_set[self.card_set.index(getattr(cards, card))]
            self.message(self.channel, "Je retire la carte " + card)
        except AttributeError:
            self.message(self.channel, "Cette carte n'existe pas")
        except ValueError:
            self.message(self.channel, "Cette carte n'est pas sélectionnée")

    def add_card(self, card):
        try:
            if getattr(cards, card) in self.card_set:
                self.message(self.channel, "Cette carte est déjà sélectionnée")
            else:
                self.card_set.append(getattr(cards, card))
                self.message(self.channel, "J'ajoute la carte " + card)
        except AttributeError:
            self.message(self.channel, "Cette carte n'existe pas")

    def list_cards(self):
        remaining = [c.__name__ for c in cards.ABSOLUTE_ORDER if c not in self.card_set]
        selected = [c.__name__ for c in self.card_set]
        msg  = "Cartes sélectionnées: " + ", ".join(selected)
        msg += "\nAutres: " + ", ".join(remaining)
        self.message(self.channel, msg)
# }}}

# {{{ game preparation
    def order_card(self):
        ''' Order and instantiate card_set regarding to ABSOLUTE_ORDER
            then and create card_dict '''
        self.card_set = [c(self) for c in cards.ABSOLUTE_ORDER if c in self.card_set]
        self.card_dict = {c.__class__.__name__: c for c in self.card_set}

    def allocate_cards(self):
        ''' Give cards to players '''
        random.shuffle(self.players)
        # Special cards
        i = 0
        for c in self.card_set:
            if c.__class__ not in [cards.LoupGarou, cards.Villageois]:
                c.player = self.players[i]
                i += 1

        # LoupGarous
        lg_number = 1 + int(len(self.players) / LGRATE)
        self.card_dict['LoupGarou'].players = self.players[i:i+lg_number]
        i += lg_number

        # Villageois
        self.card_dict['Villageois'].players = self.players[i:]
# }}}

# {{{ game tools
    def resolve_dead(self):
        pass

    def show_must_go_on(self):
        ''' Tell if the game must continue or if a team just won '''
        # TODO: Think with teams (LGs, Villageois, amoureux, flute).
        # Each team should know if it is still in the game or loosing. 
        # The game end when every team but one loose.
        # Set a "self.winner" variable, containing the winner team.
        # NOTE: this is currently an error, Villageois does not represent the
        # non-LoupGarou team (fix it quickly)
        return self.card_dict['LoupGarou'].alive and self.card_dict['Villageois'].alive and self.ingame
# }}}

# {{{ run_game
    def run_game(self):
        self.order_card()
        self.allocate_cards()

        self.message(self.channel, rdc('start'))
        self.ingame = True
        while self.show_must_go_on():
            for card in self.card_set:
                card.play()
            self.resolve_dead()

        # TODO: When show_must_go_on is implemented, just call "self.winner.win()"
        if self.card_dict['LoupGarou'].players == self.card_dict['Villageois'].players:
            self.message(self.channel, rdc('exaequo'))
        elif self.card_dict['LoupGarou'].players:
            self.message(self.channel, rdc('lgwin'))
        else:
            self.message(self.channel, rdc('vwin'))
        self.ingame = False

        # Log game state
        print("les lgs étaient {}".format(', '.join(self.card_dict['LoupGarou'].players)))
        print("les péons étaient {}".format(', '.join(self.card_dict['Villageois'].players)))
        for c in self.card_set:
            if c.__class__ not in [cards.LoupGarou, cards.Villageois]:
                print("{}: {}".format(c.__class__.__name__, c.player))

        # Make card_set a list of classes again (keep selected cards)
        self.card_set = [c.__class__ for c in self.card_set]

        # Clean players TODO: have a clean_game() function
        self.players = []
# }}}

if __name__ == '__main__':
    game = Thiercelieux()

# vim:set fdm=marker:

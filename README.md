# Werebot
See the rules of "Les loups-garous de thiercelieux"

My personal attempt to make an IRC bot for the game "Les loups garous de
Thiercelieux".

More information on the game :
http://fr.wikipedia.org/wiki/Les\_Loups-garous\_de\_Thiercelieux

This bot is based on Zopieux's pypeul library :
https://bitbucket.org/Zopieux/pypeul/

And here is a sophisticated joke:

Celui qui n'a jamais été seul
Au moins une fois dans sa vie
Peut-il seulement aimer
Peut-il aimer jamais
